package client

import (
	"context"

	"git.begroup.team/platform-core/kitchen/l"
	"git.begroup.team/platform-transport/gogi3/pb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/balancer/roundrobin"
)

type Gogi3Client struct {
	pb.Gogi3Client
}

func NewGogi3Client(address string) *Gogi3Client {
	conn, err := grpc.DialContext(context.Background(), address,
		grpc.WithInsecure(),
		grpc.WithBalancerName(roundrobin.Name),
	)

	if err != nil {
		ll.Fatal("Failed to dial Gogi3 service", l.Error(err))
	}

	c := pb.NewGogi3Client(conn)

	return &Gogi3Client{c}
}
