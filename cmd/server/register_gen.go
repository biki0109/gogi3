// IM AUTO GENERATED, BUT CAN BE OVERRIDDEN

package main

import (
	"git.begroup.team/platform-transport/gogi3/config"
	"git.begroup.team/platform-transport/gogi3/internal/services"
	"git.begroup.team/platform-transport/gogi3/internal/stores"
	"git.begroup.team/platform-transport/gogi3/pb"
)

func registerService(cfg *config.Config) pb.Gogi3Server {

	mainStore := stores.NewMainStore()

	return services.New(cfg, mainStore)
}
